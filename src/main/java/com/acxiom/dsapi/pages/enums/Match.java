/**
 * 
 */
package com.acxiom.dsapi.pages.enums;

/**
 * @Description: Match
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public enum Match {
    people(0), places(1), household(2), entities(3), batch(4);

    private final int value;

    public int getValue() {
        return value;
    }

    private Match(int value) {
        this.value = value;
    }


}
