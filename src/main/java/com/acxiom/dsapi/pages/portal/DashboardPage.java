/**
 * 
 */
package com.acxiom.dsapi.pages.portal;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;

/**
 * @Description: TODO
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public class DashboardPage extends BasePage {

    @FindBy(id = "layoutSet")
    private WebElement layoutSetButton;

    @FindBy(id = "newWidget")
    private WebElement addWidgetButton;

    @FindBy(id = "dashboardLayout")
    private WebElement dashboardLayout;

    @FindBy(xpath = "//*[contains(@class, 'widget drag panel panel-primary')]")
    private List<WebElement> widgets;

    @FindBy(xpath = "//*[@id='msgModal']/div/div/div[3]/button[1]")
    private WebElement confirmDeleteButton;

    @FindBy(xpath = "//*[@id='msgModal']/div/div/div[3]/button[2]")
    private WebElement confirmCancelButton;
    
    By widgetTitleLocator = By.className("widget-title");

    By tableLocator = By.cssSelector("table.table-bordered.table-fixed.dataTable.no-footer");
    
    By tableHeader = By.xpath("./thead/tr/th");
            
    By tableData = By.xpath("./tbody/tr");
    
    // constructor
    public DashboardPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getLayoutSetButton() {
        return layoutSetButton;
    }

    public WebElement getAddWidgetButton() {
        return addWidgetButton;
    }

    public WebElement getDashboardLayout() {
        return dashboardLayout;
    }

    public WebElement getConfirmDeleteButton() {
        return confirmDeleteButton;
    }

    public WebElement getConfirmCancelButton() {
        return confirmCancelButton;
    }

    public void clickAddWidgetButton() {
        addWidgetButton.click();
    }

    public void clickLayoutSetButton() {
        layoutSetButton.click();
    }

    public void clickConfirmDeleteButton() {
        confirmDeleteButton.click();
    }

    public void clickConfirmCancelButton() {
        confirmCancelButton.click();
    }

    public void deleteAllWidgets() throws InterruptedException {
        for (WebElement it : widgets) {
            WebElement deleteButton =
                    it.findElement(By.cssSelector(".glyphicon.glyphicon-remove.widget-remove"));
            deleteButton.click();
            waitForElement(confirmDeleteButton);
            confirmDeleteButton.click();
            waitUntil(1);
        }
    }

    public void refershWidgets() {
        //to do
    }

    public List<WebElement> getMeteringWidgets() {
        List<WebElement> meteringWidgets = new ArrayList<WebElement>();
        for (WebElement it : widgets) {
            if (it.getAttribute("class").equalsIgnoreCase(
                    "widget drag panel panel-primary meteringWidget ui-draggable ui-droppable")) {
                meteringWidgets.add(it);
            }
        }
        return meteringWidgets;
    }

    public WebElement getTodayUsageWidget() {
        WebElement todayUsageWidget = null;
        for (WebElement it : widgets) {
            if (it.getAttribute("class").equalsIgnoreCase(
                    "widget drag panel panel-primary usageTenantWidget ui-draggable ui-droppable")) {
                todayUsageWidget = it;
                break;
            }
        }
        return todayUsageWidget;
    }

    public WebElement getAPIStatusWidget() {
        WebElement apiStatusWidget = null;
        for (WebElement it : widgets) {
            if (it.getAttribute("class").equalsIgnoreCase(
                    "widget drag panel panel-primary apiStatusWidget ui-draggable ui-droppable")) {
                apiStatusWidget = it;
                break;
            }
        }
        return apiStatusWidget;
    }

    public String getWidgetTitle(String widgetName) {
        if (widgetName.equalsIgnoreCase("APIStatus")) {
            return getWidgetTitle(getAPIStatusWidget());
        }
        if (widgetName.equalsIgnoreCase("TodayUsage")) {
            return getWidgetTitle(getTodayUsageWidget());
        }
        if (widgetName.equalsIgnoreCase("Metering")) {
            return getWidgetTitle(getMeteringWidgets().get(0));
        }
        return null;
    }
    
    private String getWidgetTitle(WebElement webElement) {
        return webElement.findElement(widgetTitleLocator).getText();
    }
    
    public WebElement getFirstMeteringWidgetTable() {
        return getMeteringWidgets().get(0).findElement(tableLocator);
    }
    
    public List<WebElement> getFirstMeteringWidgetTableHeaders() {
        return getFirstMeteringWidgetTable().findElements(tableHeader);
    }
    
    public List<WebElement> getFirstMeteringWidgetTableData() {
        return getFirstMeteringWidgetTable().findElements(tableData);
    }
    
    public WebElement getAPIStatusWidgetTable() {
        return getAPIStatusWidget().findElement(tableLocator);
    }
    
    public List<WebElement> getAPIStatusWidgetTableHeaders() {
        return getAPIStatusWidget().findElements(tableHeader);
    }
    
    public List<WebElement> getAPIStatusWidgetTableData() {
        return getAPIStatusWidget().findElements(tableData);
    }
    
    public WebElement getTodayUsageWidgetTable() {
        return getTodayUsageWidget().findElement(tableLocator);
    }
    
    public List<WebElement> getTodayUsageWidgetTableHeaders() {
        return getTodayUsageWidget().findElements(tableHeader);
    }
    
    public List<WebElement> getTodayUsageWidgetTableData() {
        return getTodayUsageWidget().findElements(tableData);
    }
}
