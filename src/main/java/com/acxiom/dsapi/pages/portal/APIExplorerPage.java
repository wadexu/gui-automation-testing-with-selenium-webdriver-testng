/**
 * 
 */
package com.acxiom.dsapi.pages.portal;

import java.util.HashMap;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;
import com.acxiom.dsapi.pages.enums.Match;

/**
 * @Description: API Explorer Page
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public class APIExplorerPage extends BasePage {
    
    //match fields
    private final String FIRSTNAME = "firstName";
    private final String MIDDLENAME = "middleName";
    private final String LASTNAME = "lastName";
    private final String GENERATIONALSUFFIX = "generationalSuffix";
    private final String NAME = "name";
    private final String PRIMARYNUMBER = "primaryNumber";
    private final String PREDIRECTIONAL = "preDirectional";
    private final String STREET = "street";
    private final String STREETSUFFIX = "streetSuffix";
    private final String POSTDIRECTIONAL = "postDirectional";
    private final String UNITDESIGNATOR = "unitDesignator";
    private final String SECONDARYNUMBER = "secondaryNumber";
    private final String STREETADDRESS = "streetAddress";
    private final String CITY = "city";
    private final String STATE = "state";
    private final String ZIPCODE = "zipCode";
    private final String PERSISTEDDOCUMENTSONLY = "persistedDocumentsOnly";
    
    //metadata fields
    private final String ROLE = "role";
    private final String TENANT = "tenant";

    /**
    * <p>Title: </p>
    * <p>Description: </p>
    * @param driver
    */
    public APIExplorerPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "app_name")
    private WebElement applicationSelectBoxElement;

    @FindBy(id = "grant_type")
    private WebElement grantTypeSelectBoxElement;

    @FindBy(id = "authentiate")
    private WebElement authenticationButton;

    @FindBy(id = "app_id")
    private WebElement appKeyTextArea;

    @FindBy(id = "app_secret")
    private WebElement appSecretTextArea;

    @FindBy(id = "token_edpt")
    private WebElement tokenEndpointTextArea;

    @FindBy(id = "showAuthCode")
    private WebElement showAuthSampleCodeButton;

    @FindBy(id = "authSampleCode")
    private WebElement authSampleCodeTextArea;

    @FindBy(xpath = "//*[@id='explorer']/div/section/div[1]/h4")
    private WebElement headerElement;

    @FindBy(xpath = "//*[@id='explorer']/div/section/div[1]/label/input")
    private WebElement showAuthButton;

    @FindBy(id = "sidebar-nav")
    private WebElement sidebarNavigationList;

    @FindBy(id = "match")
    private WebElement matchAPIList;

    @FindBy(linkText = "Introduction")
    private WebElement introductionTab;

    @FindBy(linkText = "Request")
    private WebElement requestTab;

    @FindBy(linkText = "Sample Code")
    private WebElement sampleCodeTab;

    @FindBy(id = "info-dialog")
    private WebElement infoDialogElement;

    @FindBy(xpath = "//*[@id='info-dialog']/button")
    private WebElement closeInfoDialogButton;

    @FindBy(xpath = "//*[@id='11intro']/div[1]/p")
    private WebElement apiDescElement;

    @FindBy(id = "responseContentType11")
    private WebElement responseContentTypeSelectBox;

    @FindBy(css = ".table.responses")
    private WebElement responseTable;

    @FindBy(id = "param41")
    private WebElement role;
    
    @FindBy(id = "param42")
    private WebElement tenant;
    
    @FindBy(id = "param53")
    private WebElement firstNameInputBox;

    @FindBy(id = "param54")
    private WebElement middleNameInputBox;

    @FindBy(id = "param55")
    private WebElement lastNameInputBox;
    
    @FindBy(id = "param56")
    private WebElement generationalSuffix;
    
    @FindBy(id = "param57")
    private WebElement name;
    
    @FindBy(id = "param58")
    private WebElement primaryNumber;
    
    @FindBy(id = "param59")
    private WebElement preDirectional;
    
    @FindBy(id = "param60")
    private WebElement street;

    @FindBy(id = "param61")
    private WebElement streetSuffix;

    @FindBy(id = "param62")
    private WebElement postDirectional;

    @FindBy(id = "param63")
    private WebElement unitDesignator;

    @FindBy(id = "param64")
    private WebElement secondaryNumber;
    
    @FindBy(id = "param65")
    private WebElement streetAddress;
    
    @FindBy(id = "param66")
    private WebElement city;
    
    @FindBy(id = "param67")
    private WebElement state;
    
    @FindBy(id = "param68")
    private WebElement zipCode;
    
    @FindBy(id = "param69")
    private WebElement email;
    
    @FindBy(id = "param70")
    private WebElement ipAddress;
    
    @FindBy(id = "param71")
    private WebElement domain;
    
    @FindBy(id = "param72")
    private WebElement areaCode;
    
    @FindBy(id = "param73")
    private WebElement exchange;
    
    @FindBy(id = "param74")
    private WebElement subscriberNumber;
    
    @FindBy(id = "param75")
    private WebElement phone;
    
    @FindBy(id = "param76")
    private WebElement latitude;
    
    @FindBy(id = "param77")
    private WebElement longitude;
    
    @FindBy(id = "param78")
    private WebElement persistedDocumentsOnlyElement;

    @FindBy(xpath = "//*[@id='11request']/form/div[2]/button")
    private WebElement executeRequestButton;

    @FindBy(id = "11url")
    private WebElement requestURLElement;

    @FindBy(id = "11body")
    private WebElement requestBodyElement;

    @FindBy(xpath = "//*[@id='11request']/div/pre[1]")
    private WebElement responseCodeElement;

    @FindBy(xpath = "//*[@id='11request']/div/pre[2]")
    private WebElement responseHeaderElement;

    @FindBy(id = "11.java")
    private WebElement JavaSampleCodeTextArea;
    
    @FindBy(id = "11.net")
    private WebElement dotNetSampleCodeTextArea;
    
    @FindBy(id = "11.javascript")
    private WebElement JavajscriptSampleCodeTextArea;
    
    @FindBy(id = "11.python")
    private WebElement pythonSampleCodeTextArea;
    
    @FindBy(id = "11.curl")
    private WebElement curlSampleCodeTextArea;
    
    @FindBy(id = "11.ruby")
    private WebElement rubySampleCodeTextArea;
    
    @FindBy(id = "11.node")
    private WebElement nodejsSampleCodeTextArea;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[1]")
    private WebElement javaElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[2]")
    private WebElement dotNetElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[3]")
    private WebElement javascriptElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[4]")
    private WebElement pythonElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[5]")
    private WebElement curlElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[6]")
    private WebElement rubyElement;

    @FindBy(xpath = "//*[@id='11sample']/div/div/div/pre/a[7]")
    private WebElement nodeJSElement;

    public WebElement getApplicationSelectBoxElement() {
        return applicationSelectBoxElement;
    }

    public WebElement getGrantTypeSelectBoxElement() {
        return grantTypeSelectBoxElement;
    }

    public WebElement getAuthenticationButton() {
        return authenticationButton;
    }

    public WebElement getAppKeyTextArea() {
        return appKeyTextArea;
    }

    public WebElement getAppSecretTextArea() {
        return appSecretTextArea;
    }

    public WebElement getTokenEndpointTextArea() {
        return tokenEndpointTextArea;
    }

    public WebElement getShowAuthSampleCodeButton() {
        return showAuthSampleCodeButton;
    }

    public WebElement getAuthSampleCodeTextArea() {
        return authSampleCodeTextArea;
    }

    public WebElement getHeaderElement() {
        return headerElement;
    }

    public WebElement getShowAuthButton() {
        return showAuthButton;
    }

    public WebElement getSidebarNavigationList() {
        return sidebarNavigationList;
    }

    public WebElement getMatchAPIList() {
        return matchAPIList;
    }

    public WebElement getIntroductionTab() {
        return introductionTab;
    }

    public WebElement getRequestTab() {
        return requestTab;
    }

    public WebElement getSampleCodeTab() {
        return sampleCodeTab;
    }

    public WebElement getInfoDialogElement() {
        return infoDialogElement;
    }

    public WebElement getCloseInfoDialogButton() {
        return closeInfoDialogButton;
    }

    public WebElement getApiDescElement() {
        return apiDescElement;
    }

    public WebElement getResponseContentTypeSelectBox() {
        return responseContentTypeSelectBox;
    }

    public WebElement getResposneTable() {
        return responseTable;
    }

    public WebElement getFirstNameInputBox() {
        return firstNameInputBox;
    }

    public WebElement getMiddleNameInputBox() {
        return middleNameInputBox;
    }

    public WebElement getLastNameInputBox() {
        return lastNameInputBox;
    }
    
    public WebElement getGenerationalSuffix() {
        return generationalSuffix;
    }

    public WebElement getName() {
        return name;
    }

    public WebElement getPrimaryNumber() {
        return primaryNumber;
    }

    public WebElement getPreDirectional() {
        return preDirectional;
    }

    public WebElement getStreet() {
        return street;
    }

    public WebElement getStreetSuffix() {
        return streetSuffix;
    }

    public WebElement getPostDirectional() {
        return postDirectional;
    }

    public WebElement getUnitDesignator() {
        return unitDesignator;
    }

    public WebElement getSecondaryNumber() {
        return secondaryNumber;
    }

    public WebElement getStreetAddress() {
        return streetAddress;
    }

    public WebElement getCity() {
        return city;
    }

    public WebElement getState() {
        return state;
    }

    public WebElement getZipCode() {
        return zipCode;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getIpAddress() {
        return ipAddress;
    }

    public WebElement getDomain() {
        return domain;
    }

    public WebElement getAreaCode() {
        return areaCode;
    }

    public WebElement getExchange() {
        return exchange;
    }

    public WebElement getSubscriberNumber() {
        return subscriberNumber;
    }

    public WebElement getPhone() {
        return phone;
    }

    public WebElement getLatitude() {
        return latitude;
    }

    public WebElement getLongitude() {
        return longitude;
    }

    public WebElement getPersistedDocumentsOnlySelectBox() {
        return persistedDocumentsOnlyElement;
    }

    public WebElement getExecuteRequestButton() {
        return executeRequestButton;
    }

    public WebElement getRequestURLElement() {
        return requestURLElement;
    }

    public WebElement getRequestBodyElement() {
        return requestBodyElement;
    }

    public WebElement getResponseCodeElement() {
        return responseCodeElement;
    }

    public WebElement getResponseHeaderElement() {
        return responseHeaderElement;
    }

    public WebElement getJavaSampleCodeTextArea() {
        return JavaSampleCodeTextArea;
    }
    
    public WebElement getDotNetSampleCodeTextArea() {
        return dotNetSampleCodeTextArea;
    }

    public WebElement getJavajscriptSampleCodeTextArea() {
        return JavajscriptSampleCodeTextArea;
    }

    public WebElement getPythonSampleCodeTextArea() {
        return pythonSampleCodeTextArea;
    }

    public WebElement getCURLSampleCodeTextArea() {
        return curlSampleCodeTextArea;
    }

    public WebElement getRubySampleCodeTextArea() {
        return rubySampleCodeTextArea;
    }

    public WebElement getNodejsSampleCodeTextArea() {
        return nodejsSampleCodeTextArea;
    }

    public WebElement getJavaElement() {
        return javaElement;
    }

    public WebElement getDotNetElement() {
        return dotNetElement;
    }

    public WebElement getJavascriptElement() {
        return javascriptElement;
    }

    public WebElement getPythonElement() {
        return pythonElement;
    }

    public WebElement getCurlElement() {
        return curlElement;
    }

    public WebElement getRubyElement() {
        return rubyElement;
    }

    public WebElement getNodeJSElement() {
        return nodeJSElement;
    }

    public void clickShowAuthenticationCheckBox() {
        showAuthButton.click();
    }
    
    public void clickAuthenticationButton() {
        authenticationButton.click();
    }
    
    public void selectApplication(String value) {
        getSelectBox(applicationSelectBoxElement).selectByVisibleText(value);
    }

    public void selectGrantType(String value) {
        getSelectBox(grantTypeSelectBoxElement).selectByVisibleText(value);
    }

    public void getAuthentication(String app, String type) {
        selectApplication(app);
        selectGrantType(type);
        clickAuthenticationButton();
    }
    
    public void clickAuthSampleCodeButton() {
        showAuthSampleCodeButton.click();
    }
    
    public void clickMatchAPI(Match match) {
        switch (match) {
            case people:
                clickMatchAPI(Match.people.getValue());
                break;

            case places:
                clickMatchAPI(Match.places.getValue());
                break;

            case household:
                clickMatchAPI(Match.household.getValue());
                break;

            case entities:
                clickMatchAPI(Match.entities.getValue());
                break;

            case batch:
                clickMatchAPI(Match.batch.getValue());
                break;
        }

    }

    private void clickMatchAPI(int num) {
        matchAPIList.findElements(By.tagName("li")).get(num).click();
    }
    
    public void sendMatchRequest(HashMap<String, String> record) {
        input(firstNameInputBox, record.get(FIRSTNAME));
        input(middleNameInputBox, record.get(MIDDLENAME));
        input(lastNameInputBox, record.get(LASTNAME));
        input(generationalSuffix, record.get(GENERATIONALSUFFIX));
        input(name, record.get(NAME));
        input(primaryNumber, record.get(PRIMARYNUMBER));
        input(preDirectional, record.get(PREDIRECTIONAL));
        input(street, record.get(STREET));
        input(streetSuffix, record.get(STREETSUFFIX));
        input(postDirectional, record.get(POSTDIRECTIONAL));
        input(unitDesignator, record.get(UNITDESIGNATOR));
        input(secondaryNumber, record.get(SECONDARYNUMBER));
        input(streetAddress, record.get(STREETADDRESS));
        input(city, record.get(CITY));
        input(state, record.get(STATE));
        input(zipCode, record.get(ZIPCODE));
        
        //default is true, if input is false, select to false
        if (record.get(PERSISTEDDOCUMENTSONLY).equalsIgnoreCase("false")) {
            selectPersistedDocumentsOnly("false");
        }
        
        clickExecuteRequestButton();
    }
    
    public void sendMetaDataRequest(HashMap<String, String> record) {
        input(role, record.get(FIRSTNAME));
        input(tenant, record.get(MIDDLENAME));
        
        clickExecuteRequestButton();
    }

    public void selectPersistedDocumentsOnly(String value) {
        getSelectBox(persistedDocumentsOnlyElement).selectByValue(value);
    }
    
    public void clickExecuteRequestButton() {
        executeRequestButton.click();
    }
    
    public String getTextOfRequestURL() {
        return requestURLElement.getText();
    }
    
    public String getTextOfResponseBody() {
        return requestBodyElement.getText();
    }
    
    public String getTextOfResponseCode() {
        return responseCodeElement.getText();
    }
    
    public String getTextOfResponseHeaders() {
        return responseHeaderElement.getText();
    }
    
    public String getJavaSampleCode() {
        javaElement.click();
        return JavaSampleCodeTextArea.getText();
    }
    
    public String getDotNetSampleCode() {
        dotNetElement.click();
        return dotNetSampleCodeTextArea.getText();
    }
    
    public String getJavascriptSampleCode() {
        javascriptElement.click();
        return JavajscriptSampleCodeTextArea.getText();
    }
    
    public String getPythonSampleCode() {
        pythonElement.click();
        return pythonSampleCodeTextArea.getText();
    }
    
    public String getCurlSampleCode() {
        curlElement.click();
        return curlSampleCodeTextArea.getText();
    }
    
    public String getRubySampleCode() {
        rubyElement.click();
        return rubySampleCodeTextArea.getText();
    }
    
    public String getNodeJsSampleCode() {
        nodeJSElement.click();
        return nodejsSampleCodeTextArea.getText();
    }
    
    public String getTextOfKey() {
        return appKeyTextArea.getText();
    }
    
    public String getTextOfSecret() {
        return appSecretTextArea.getText();
    }
    
    public String getTextOfTokenEndpoint() {
        return tokenEndpointTextArea.getText();
    }
    
    public void naviagetToReuqestTab() {
        requestTab.click();
    }
    
    public void naviagetToSampleCodeTab() {
        sampleCodeTab.click();
    }
    
    public void naviagetToIntroductionTab() {
        introductionTab.click();
    }
}
