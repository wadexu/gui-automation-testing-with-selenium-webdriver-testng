/**
 * 
 */
package com.acxiom.dsapi.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @Description: BasePage
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public abstract class BasePage {
    private static final Logger LOGGER = LoggerFactory.getLogger(BasePage.class);
    private WebDriver webDriver;
    private static final int TIMEOUT_LIMIT = 60000;
    private static final int SLEEP_TIME = 1000;

    public BasePage(WebDriver driver) {
        AjaxElementLocatorFactory finder = new AjaxElementLocatorFactory(driver, 10);
        PageFactory.initElements(finder, this);
        
        webDriver = driver;
    }
    
    public void waitForSpinner(WebElement element) {
        // and now waiting for spinner disappearance
        int i = 0;
        while ((element.isDisplayed()) && i <= TIMEOUT_LIMIT){
            webDriver.manage().timeouts().implicitlyWait(100, TimeUnit.MILLISECONDS);
          i +=100;
        }
      }

      public void waitForElement(WebElement element) throws InterruptedException {
        int i = 0;
        while (!isElementPresent(element) && i <= TIMEOUT_LIMIT) {
          Thread.sleep(SLEEP_TIME);
          i += SLEEP_TIME;
        }
      }

      public void waitForElementDisappear(WebElement element) {
        WebDriverWait wait = new WebDriverWait(webDriver, 1000);
        while (isElementPresent(element)) {
          wait.until(ExpectedConditions.not(ExpectedConditions.visibilityOf(element)));
        }
      }
      
      public boolean isElementPresent(By by) {
          return isElementPresent(webDriver.findElement(by));
      }

      public boolean isElementPresent(WebElement element) {
        try {
          return element.isEnabled() && element.isDisplayed();
        } catch (WebDriverException e) {
          return false;
        }
      }

      /**
       * Try to wait on the current <code>Page</code> (and current <code>WebDriver</code> state) while
       * <code>WebElement</code> does not found by locator during timeout.
       * 
       * @param by The locating mechanism
       * @param timeout , timeout to lookup element in seconds
       * @return boolean if element is still present.
       * @see org.openqa.selenium.By
       */
      public boolean waitUntilElementDisappear(By by, int timeout) {
        if(isElementPresent(by)) {
          waitUntil(timeout);
        }
        return isElementPresent(by);
      }

      public boolean waitUntilElementDisappear(WebElement element, int timeout) {
          if(isElementPresent(element)) {
            waitUntil(timeout);
          }
          return isElementPresent(element);
        }
      
      /**
       * Thread sleep method
       * 
       * @param timeout Seconds
       */
      public void waitUntil(int timeout) {
        try {
          Thread.sleep(timeout * 1000);
        } catch(InterruptedException e) {
          LOGGER.error("Exception while wait until method work", e);
        }
      }
      
      public Select getSelectBox(WebElement element) {
          Select appSelectBox = new Select(element);
          return appSelectBox;
      }
      
      public void input(WebElement webElement, String value) {
          webElement.sendKeys(value);
      }
}
