/**
 * 
 */
package com.acxiom.dsapi.pages.dialog;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;

/**
 * @Description: TODO
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class DashboardWidgetDirDialog extends BasePage {

    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[2]/div[2]/div[1]/div[1]/div[2]/button")
    private WebElement addMeteringWidgetButton;
    
    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[2]/div[2]/div[2]/div[1]/div[2]/button")
    private WebElement addUsageWidgetButton;
    
    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[2]/div[2]/div[3]/div[1]/div[2]/button")
    private WebElement addAPIStatusWidgetButton;
    
    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[2]/div[2]/div[4]/div[1]/div[2]/button")
    private WebElement addResponseTimeWidgetButton;
   
    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[3]/button")
    private WebElement closeButton;
  
    @FindBy(css = ".col-md-3.widget-nav.li > ul > li")
    private List<WebElement> widgetNavbar;
    
    @FindBy(xpath = "//*[@id='widgetModal']/div/div/div[1]/h4")
    private WebElement widgetTitle;
    
    @FindBy(xpath = "//div[@id='widgetModal']//div[contains(@class, 'alert alert-dismissible ng-scope alert-warning')]")
    private WebElement addWidgetInfo;
    
    @FindBy(xpath = "//div[@id='widgetModal']//div[contains(@class, 'alert alert-dismissible ng-scope alert-danger')]")
    private WebElement addWidgetError;
    
    public DashboardWidgetDirDialog(WebDriver driver) {
        super(driver);
    }
    
    public WebElement getAddMeteringWidgetButton() {
        return addMeteringWidgetButton;
    }

    public WebElement getAddUsageWidgetButton() {
        return addUsageWidgetButton;
    }

    public WebElement getAddAPIStatusWidgetButton() {
        return addAPIStatusWidgetButton;
    }

    public WebElement getAddResponseTimeWidgetButton() {
        return addResponseTimeWidgetButton;
    }

    public WebElement getCloseButton() {
        return closeButton;
    }

    public List<WebElement> getWidgetNavbar() {
        return widgetNavbar;
    }
    
    public WebElement getWidgetTitle() {
        return widgetTitle;
    }
    
    public WebElement getAddWidgetInfo() {
        return addWidgetInfo;
    }
    
    public void clickAddMeteringWidgetButton() {
        addMeteringWidgetButton.click();
    }
    
    public void clickAddUsageWidgetButton() {
        addUsageWidgetButton.click();
    }
    
    public void clickAddAPIStatusWidgetButton() {
        addAPIStatusWidgetButton.click();
    }
    
    public void clickAddResponseTimeWidgetButton() {
        addResponseTimeWidgetButton.click();
    }
    
    public void clickCloseButton() {
        closeButton.click();
    }
    
    public void closeWidgetInfoAlert() {
        addWidgetInfo.findElement(By.className("close")).click();
    }
    
    public String getWidgetInfoMsg() {
        return addWidgetInfo.findElement(By.tagName("p")).getText();
    }
    
    public void closeWidgetErrorAlert() {
        addWidgetError.findElement(By.className("close")).click();
    }
    
    public String getWidgetErrorMsg() {
        return addWidgetError.findElement(By.tagName("p")).getText();
    }
    
}
