/**
 * 
 */
package com.acxiom.dsapi.pages.dialog;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;

/**
 * @Description: Choose Dashboard Layout Dialog
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class DashboardLayoutDialog extends BasePage {

    @FindBy(id = "layout-a")
    private WebElement layout_a;
    
    @FindBy(id = "layout-aa")
    private WebElement layout_aa;
    
    @FindBy(id = "layout-ba")
    private WebElement layout_ba;
    
    @FindBy(id = "layout-ab")
    private WebElement layout_ab;
    
    @FindBy(id = "layout-aaa")
    private WebElement layout_aaa;
    
    @FindBy(xpath = "//*[@id='dashboardModal']/div/div/div[3]/button")
    private WebElement closeButton;
    
    @FindBy(xpath = "//*[@id='dashboardModal']/div/div/div[1]/h4")
    private WebElement layoutTitle;
    
    /**
    * <p>Title: </p>
    * <p>Description: </p>
    * @param driver
    */
    public DashboardLayoutDialog(WebDriver driver) {
        super(driver);
    }

    public WebElement getLayout_a() {
        return layout_a;
    }

    public WebElement getLayout_aa() {
        return layout_aa;
    }

    public WebElement getLayout_ba() {
        return layout_ba;
    }

    public WebElement getLayout_ab() {
        return layout_ab;
    }

    public WebElement getLayout_aaa() {
        return layout_aaa;
    }

    public WebElement getCloseButton() {
        return closeButton;
    }
    
    public WebElement getLayoutTitle() {
        return layoutTitle;
    }
}
