/**
 * 
 */
package com.acxiom.dsapi.pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;

/**
 * @Description: LoginPage
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public class LoginPage extends BasePage {

    @FindBy(id = "loginUsername")
    private WebElement userName;

    @FindBy(name = "j_password")
    private WebElement password;

    @FindBy(xpath = "//*[@id='loginCtrl']/div[2]/div[2]/div[2]/form/div[4]/button")
    private WebElement loginButton;

    @FindBy(xpath = "//*[@id='loginCtrl']/div[2]/div[2]/div[2]/div[3]/button")
    private WebElement resetPassword;

    @FindBy(xpath = "//*[@id='loginCtrl']/div[2]/div[2]/div[2]/div[2]")
    private WebElement failureText;
    
    By image = By.xpath("//*[@src='../images/loginKey.png']");
    
    public By getImage() {
        return image;
    }
    
    /**
    * Constructor 
    * <p>Title: </p>
    * <p>Description: </p>
    * @param driver
    */
    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getUserName() {
        return userName;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public WebElement getResetPassword() {
        return resetPassword;
    }

    public WebElement getFailureText() {
        return failureText;
    }

    public LoginPage typeUsername(String username) {
        this.userName.clear();
        this.userName.sendKeys(username);
        return this;
    }

    public LoginPage typePassword(String password) {
        this.password.clear();
        this.password.sendKeys(password);
        return this;
    }

    public void submitLogin() {
        loginButton.submit();
    }

    public void loginAs(String username, String password) {
        typeUsername(username);
        typePassword(password);
        submitLogin();
    }
}
