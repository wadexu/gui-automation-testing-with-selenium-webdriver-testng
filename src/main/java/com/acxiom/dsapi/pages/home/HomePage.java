/**
 * 
 */
package com.acxiom.dsapi.pages.home;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import com.acxiom.dsapi.pages.BasePage;

/**
 * @Description: HomePage
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class HomePage extends BasePage{

    @FindBy(id = "home")
    private WebElement homeMenu;
    
    @FindBy(linkText = "/login")
    private WebElement loginLink;
    
    @FindBy(linkText = "/register")
    private WebElement registerLink;
    
    @FindBy(id = "learnNav")
    private WebElement learnMainLink;
    
    @FindBy(id = "codeNav")
    private WebElement codeMainLink;

    @FindBy(id = "launchNav")
    private WebElement launchMainLink;
    
    @FindBy(xpath = "//*[@href='/dashboard'][(@data-delay='300')]")
    private WebElement dashboardMainLink;
    
    @FindBy(linkText = "/admin/#/messages")
    private WebElement adminConsoleLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[3]/div/div/div[1]/ul/li[1]/small/a")
    private WebElement dataLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[3]/div/div/div[1]/ul/li[2]/small/a")
    private WebElement servicesLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[3]/div/div/div[1]/ul/li[3]/small/a")
    private WebElement useCasesLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[3]/div/div/div[1]/ul/li[4]/small/a")
    private WebElement contactusLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[4]/div/div/div[2]/ul/li[1]/small/a")
    private WebElement gettingStartedLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[4]/div/div/div[2]/ul/li[2]/small/a")
    private WebElement apiDocsLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[4]/div/div/div[2]/ul/li[3]/small/a")
    private WebElement sendYourFirstRequestLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[4]/div/div/div[2]/ul/li[4]/small/a")
    private WebElement toolsLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[4]/div/div/div[2]/ul/li[5]/small/a")
    private WebElement codeSamplesLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[5]/div/div/div[1]/ul/li[1]/small/a")
    private WebElement requestProdKey;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[5]/div/div/div[1]/ul/li[2]/small/a")
    private WebElement supportLink;
    
    @FindBy(xpath = "//*[@id='bg-boxed']/div/div/div[5]/div/div/div[1]/ul/li[3]/small/a")
    private WebElement communityLink;
    
    @FindBy(linkText = "Add an Application")
    private WebElement addApplication;
    
    @FindBy(xpath = "//*[@id='#']/a/span")
    private WebElement welcomeLabel;
    
    @FindBy(xpath = "//*[@id='codeNav']/ul/li[1]/a")
    private WebElement gettingStartedSubMenu;
    
    @FindBy(xpath = "//*[@id='codeNav']/ul/li[1]/ul/li[3]/a")
    private WebElement addApplicationSubMenu;
    
    By learnPicLocator = By.xpath("//*[@id='bg-boxed']/div/div/div[2]/div/div/div[1]/a/h4");
    
    /**
    * <p>Title: </p>
    * <p>Description: </p>
    * @param driver
    */
    public HomePage(WebDriver driver) {
        super(driver);
    }

    public WebElement getHomeMenu() {
        return homeMenu;
    }

    public WebElement getLoginLink() {
        return loginLink;
    }

    public WebElement getRegisterLink() {
        return registerLink;
    }

    public WebElement getLearnMainLink() {
        return learnMainLink;
    }

    public WebElement getCodeMainLink() {
        return codeMainLink;
    }

    public WebElement getLaunchMainLink() {
        return launchMainLink;
    }

    public WebElement getDashboardLink() {
        return dashboardMainLink;
    }

    public WebElement getAdminConsoleLink() {
        return adminConsoleLink;
    }

    public WebElement getDataLink() {
        return dataLink;
    }

    public WebElement getServicesLink() {
        return servicesLink;
    }

    public WebElement getUseCasesLink() {
        return useCasesLink;
    }

    public WebElement getContactusLink() {
        return contactusLink;
    }

    public WebElement getGettingStartedLink() {
        return gettingStartedLink;
    }

    public WebElement getApiDocsLink() {
        return apiDocsLink;
    }

    public WebElement getSendYourFirstRequestLink() {
        return sendYourFirstRequestLink;
    }

    public WebElement getToolsLink() {
        return toolsLink;
    }

    public WebElement getCodeSamplesLink() {
        return codeSamplesLink;
    }

    public WebElement getRequestProdKey() {
        return requestProdKey;
    }

    public WebElement getSupportLink() {
        return supportLink;
    }

    public WebElement getCommunityLink() {
        return communityLink;
    }
    
    public WebElement getAddApplication() {
        return addApplication;
    }
    
    public WebElement getWelcomeLabel() {
        return welcomeLabel;
    }
    
    public WebElement getGettingStartedSubMenu() {
        return gettingStartedSubMenu;
    }

    public WebElement getAddApplicationSubMenu() {
        return addApplicationSubMenu;
    }
    
    public By getLearnPicLocator() {
        return learnPicLocator;
    }


}
