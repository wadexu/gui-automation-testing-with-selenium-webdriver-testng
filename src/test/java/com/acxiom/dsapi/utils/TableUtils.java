/**
 * 
 */
package com.acxiom.dsapi.utils;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * @Description: Table handle Utils
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class TableUtils {

    /**
     * @param i column position in row
     * @return list of cell contains
     */
    public static List<String> getStringValueByColumnPosition(List<WebElement> rows, int column) {
      // create empty result list
      List<String> result = new ArrayList<String>();
      // check each rows and get column value by index
      for (WebElement w : rows) {
        // find column
        WebElement element = w.findElement(By.xpath("./td[" + column + "]"));
        // get value
        result.add(element.getText());
      }
      return result;
    }
}
