/**
 * 
 */
package com.acxiom.dsapi.tests;

import static org.testng.Assert.assertEquals;

import java.util.concurrent.TimeUnit;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acxiom.dsapi.pages.home.LoginPage;
import com.acxiom.qa.core.testng.TakeScreenshotOnFailure;

/**
 * @Description: Test Login Page
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class TestLoginPage extends TestBase {
    private LoginPage loginPage;

    @BeforeMethod
    public void beforeTest() {
      webDriver.get(loginURL);
      webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
      loginPage = new LoginPage(webDriver);
    }

    @Test
    @TakeScreenshotOnFailure
    public void testIvalidLoginPassword() {
      loginPage.loginAs("wade", "wade");
      waitTextToBePresentInElement(loginPage.getFailureText(), "Invalid user name or password.");
      assertEquals(loginPage.getFailureText().getText().trim(), "Invalid user name or password.");
    }

    @Test
    @TakeScreenshotOnFailure
    public void testSuccessLogin() {
      loginPage.loginAs(login, password);
      waitUntilInvisible(loginPage.getImage());
      assertEquals(webDriver.getCurrentUrl(), "http://accounts.myacxiom.loc/#/UserInfo");
    }
}
