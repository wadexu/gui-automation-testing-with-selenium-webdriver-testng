/**
 * 
 */
package com.acxiom.dsapi.tests;

import com.acxiom.dsapi.pages.home.LoginPage;

/**
 * @Description: LoginableTestBase
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public class LoginableTestBase extends TestBase {

    private LoginPage loginPage;

    public void doLogin() {
        webDriver.get(loginURL);
        loginPage = new LoginPage(webDriver);
        loginPage.loginAs(login, password);
        waitUntilInvisible(loginPage.getImage());
    }

}
