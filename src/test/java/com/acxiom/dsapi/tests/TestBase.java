/**
 * 
 */
package com.acxiom.dsapi.tests;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import com.acxiom.qa.core.utils.PropLoader;
import com.acxiom.qa.core.utils.QaCoreException;
import com.acxiom.qa.core.utils.WebDriverFactoryEx;
import com.acxiom.qa.core.utils.WebDriverHost;
import com.acxiom.qa.qa_common.utils.CommonCapabilityFactory;

/**
 * @Description: TestBase
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public abstract class TestBase implements WebDriverHost {

    private static final long DEFAULT_TIMEOUT = 1;
    private static final Logger LOGGER = LoggerFactory.getLogger(TestBase.class);
    
    protected WebDriver webDriver = null;
    protected PropLoader props = null;
    protected Actions actions = null;
    protected FluentWait<WebDriver> wait = null;
    protected String baseUrl = "";
    protected String loginURL = "";
    protected String passwordResetURL = "";
    protected String login = "";
    protected String password = "";

    @Override
    public WebDriver getWebDriver() {
        return webDriver;
    }

    @Parameters({"browser", "env"})
    @BeforeClass
    public void beforeClass(String browser, String env) throws IOException, QaCoreException {
        //
        org.apache.commons.logging.LogFactory.getFactory().setAttribute(
                "org.apache.commons.logging.Log", "org.apache.commons.logging.impl.NoOpLog");
        //
        WebDriverFactoryEx.BrowserType bt =
                WebDriverFactoryEx.BrowserType.valueOf(WebDriverFactoryEx.BrowserType.class,
                        browser);

        DesiredCapabilities desiredCapabilities = CommonCapabilityFactory.createCapability(bt);
        
        if ("firefox".equalsIgnoreCase(bt.name())) {
            FirefoxProfile profile = new FirefoxProfile();
           
            profile.addExtension(new File(getClass().getClassLoader().getResource("no_google_analytics-0.6-an+fx.xpi").getFile()));
            profile.setPreference("network.proxy.type", 0);
            desiredCapabilities.setCapability(FirefoxDriver.PROFILE, profile);

        }
        
        // create webDriver
        webDriver = WebDriverFactoryEx.createWebDriver(desiredCapabilities);

        // Sets initial size of browser window
        webDriver.manage().window().maximize();

        // Dimension dim = new Dimension(1280, 1300);
        // webDriver.manage().window().setSize(dim);

        // Sets default wait timeout for findElement() calls
        webDriver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        // Initializes wait and sets default wait timeout for wait.until() calls (in # of seconds)
        // wait = new WebDriverWait(webDriver, 10);
        wait =
                new FluentWait<WebDriver>(webDriver).withTimeout(10, TimeUnit.SECONDS)
                        .pollingEvery(1, TimeUnit.SECONDS).ignoring(NoSuchElementException.class);
        actions = new Actions(webDriver);
        if (env.equals("QA")) {
            baseUrl = PropLoader.getProps().getProperty("DS_qa_url");
            login = PropLoader.getProps().getProperty("DS_qa_login");
            password = PropLoader.getProps().getProperty("DS_qa_password");
            loginURL = PropLoader.getProps().getProperty("DS_qa_login_url");
            passwordResetURL = PropLoader.getProps().getProperty("DS_qa_password_reset_url");
        }
        if (env.equals("DEV")) {
            baseUrl = PropLoader.getProps().getProperty("DS_dev_url");
            login = PropLoader.getProps().getProperty("DS_dev_login");
            password = PropLoader.getProps().getProperty("DS_dev_password");
            loginURL = PropLoader.getProps().getProperty("DS_dev_login_url");
            passwordResetURL = PropLoader.getProps().getProperty("DS_dev_password_reset_url");
        }

    }

    @Parameters({"browser", "env"})
    @AfterClass(alwaysRun = true)
    public void tearDown(String browser, String env) {
//        if (webDriver != null) {
//            webDriver.close();
//            webDriver.quit();
//        }
    }

    public void waitUntilInvisible(By by) {
        new WebDriverWait(webDriver, 10).until(ExpectedConditions.invisibilityOfElementLocated(by));

    }

    public void waitUntilBecomesVisible(WebElement webElement) {
        new WebDriverWait(webDriver, 10).until(ExpectedConditions.visibilityOf(webElement));
    }
    
    public void waitUntilVisibleOfElementLocated(By by) {
        new WebDriverWait(webDriver, 10).until(ExpectedConditions.visibilityOfElementLocated(by));
    }

    public void waitTextToBePresentInElement(WebElement element, String text) {
        new WebDriverWait(webDriver, 10).until(ExpectedConditions.textToBePresentInElement(element, text));
    }
    
    public Boolean WaitUntilVisible(WebElement element) {

        WebDriverWait wait = new WebDriverWait(webDriver, 15);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public Boolean WaitUntilClickable(WebElement element) {

        WebDriverWait wait = new WebDriverWait(webDriver, 15);
        try {
            wait.until(ExpectedConditions.elementToBeClickable(element));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public Boolean WaitUntilPresent(By locator) {

        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        try {
            wait.until(ExpectedConditions.presenceOfElementLocated(locator));
        } catch (Exception e) {
            return false;
        }

        return true;
    }

    public void clickWebElement(WebElement element) {
        ((JavascriptExecutor) webDriver).executeScript("arguments[0].click();", element);
      }
    
    /**
     * Method to find out if element exists on the page.
     * 
     * @param by , <code>By</code> - <code>WebElement</code> locator
     * @return <b>boolean</b><code>true</code> if element found by locator <code>By</code>
     * @see By
     * @see WebElement
     */
    public boolean isElementPresent(By by) {
      try {
        findElementByTimeout(by, 5);
        return true;
      } catch(NoSuchElementException e) {
        return false;
      }
    }

    /**
     * Try to find {@link WebElement} of the current <code>Page</code> (and current <code>WebDriver</code> state) by
     * locator and timeout. If there is no such element or timeout happened it throws {@link NoSuchElementException}
     * 
     * @param by The locating mechanism
     * @param timeout , timeout to lookup element in seconds
     * @return The first matching element on the current page
     * @throws NoSuchElementException If no matching elements are found or if timeout happen
     * @see org.openqa.selenium.By
     */
    public WebElement findElementByTimeout(By by, int timeout) {
      webDriver.manage().timeouts().implicitlyWait(timeout, TimeUnit.SECONDS);
      WebElement webElement = getWebDriver().findElement(by);
      webDriver.manage().timeouts().implicitlyWait(DEFAULT_TIMEOUT, TimeUnit.SECONDS);
      return webElement;
    }
    
    /**
     * Try to wait on the current <code>Page</code> (and current <code>WebDriver</code> state) while
     * <code>WebElement</code> found by locator during timeout.
     * 
     * @param by The locating mechanism
     * @param timeout , timeout to lookup element in seconds
     * @see org.openqa.selenium.By
     */
    public void waitUntilElementExists(By by, int timeout) {
      long current = System.currentTimeMillis();
      WebDriverWait wait = new WebDriverWait(getWebDriver(), timeout);
      wait.until(ExpectedConditions.visibilityOfElementLocated(by));
      LOGGER.info("ELEMENT " + by + " found by " + (System.currentTimeMillis() - current) / 1000 + " seconds");
    }

    /**
     * Try to wait on the current <code>Page</code> (and current <code>WebDriver</code> state) while
     * <code>WebElement</code> does not found by locator during timeout.
     * 
     * @param by The locating mechanism
     * @param timeout , timeout to lookup element in seconds
     * @return boolean if element is still present.
     * @see org.openqa.selenium.By
     */
    public boolean waitUntilElementDisappear(By by, int timeout) {
      if(isElementPresent(by)) {
        waitUntil(timeout);
      }
      return isElementPresent(by);
    }
    
    /**
     * Thread sleep method
     * 
     * @param timeout Seconds
     */
    public void waitUntil(int timeout) {
      try {
        Thread.sleep(timeout * 1000);
      } catch(InterruptedException e) {
        LOGGER.error("Exception while wait until method work", e);
      }
    }
    
    public boolean waitForBrowserFullSync() {
        final String currentbowserstate = (String) executeJS("return document.readyState;");
        LOGGER.info("Current browser state is:" + currentbowserstate);
        WebDriverWait wdw = new WebDriverWait(webDriver, 300);
        ExpectedCondition<Boolean> ec = new ExpectedCondition<Boolean>() {
            @Override
            public Boolean apply(WebDriver driver) {
                // TODO Auto-generated method stub
                String newpagestate = (String) executeJS("return document.readyState;");
                LOGGER.debug("the new page state is:" + newpagestate);
                return (newpagestate.equals("complete"));
            }
        };

        boolean loaded = wdw.until(ec);
        LOGGER.debug("finally the load is loading with completed status is:"
                + loaded);
        return loaded;
    }
    
    public void executeJS(String script,WebElement e) {
        JavascriptExecutor je = (JavascriptExecutor) webDriver;
        je.executeScript(script,e);

    }

    public Object executeJS(String script) {
        JavascriptExecutor je = (JavascriptExecutor) webDriver;
        return je.executeScript(script);
    }
    
    public void setPageLoadTimeOut(long time) {
        webDriver.manage().timeouts().pageLoadTimeout(time, TimeUnit.SECONDS);
   }
    
    /**
     * Wait for web page title change to string that contains provided phrase.
     * 
     * @param title - web page title phrase to wait for.
     * @param timeoutSeconds - timeout for passive wait for element.
     * @return true/false depending on result.
     */
    protected Boolean WaitUntilTitleContains(String title) {

      WebDriverWait wait = new WebDriverWait(webDriver, 20);
      try {
        return wait.until(ExpectedConditions.titleContains(title));
      } catch(Exception e) {
        return false;
      }
    }
    
//    public Boolean WaitUntilTitleContains(String title) {
//
//        WebDriverWait wait = new WebDriverWait(webDriver, 20);
//        try {
//            wait.until(ExpectedConditions.titleContains(title));
//        } catch (Exception e) {
//            if (webDriver.getTitle().equalsIgnoreCase(title)) {
//                return true;
//            } else {
//                return false;
//            }
//        }
//        return true;
//    }
}
