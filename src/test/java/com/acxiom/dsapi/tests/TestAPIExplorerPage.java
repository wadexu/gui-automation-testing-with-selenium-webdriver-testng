package com.acxiom.dsapi.tests;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.acxiom.dsapi.pages.enums.Match;
import com.acxiom.dsapi.pages.portal.APIExplorerPage;
import com.acxiom.qa.core.utils.api.DataHandler;
import com.acxiom.qa.core.utils.api.RecordHandler;

/**
 * @Description: Test API Explorer Page
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class TestAPIExplorerPage extends LoginableTestBase {

    private APIExplorerPage apiExplorerPage;
    private DataHandler myData;
    private DataHandler myBaselineData;
    private Map<String, RecordHandler> myInput;
    
    @BeforeClass(dependsOnMethods = {"beforeClass"})
    public void init() {
        doLogin();
        // init page
        webDriver.get(baseUrl + "/code/explorer");
        apiExplorerPage = new APIExplorerPage(webDriver);
        waitUntil(1);
        apiExplorerPage.getAuthentication("connect", "Client Credential");
        assertEquals(apiExplorerPage.getTextOfKey(), "");
        
        //init csv data
        dataInitialization();
    }

    @Test(dataProvider="dataProviderForMatch", description="People Match Test")
    public void generatePeopleMatchRequest(String ID, String test_case) {
        apiExplorerPage.clickMatchAPI(Match.people);
        apiExplorerPage.naviagetToReuqestTab();
        HashMap<String, String> record = myData.get_record(ID).get_map();
        
        apiExplorerPage.sendMatchRequest(record);
        assertResult(ID);
    }

    @Test(dataProvider="dataProviderForMatch", description="Place Match Test")
    public void generatePlaceMatchRequest(String ID, String test_case) {
        apiExplorerPage.clickMatchAPI(Match.places);
        apiExplorerPage.naviagetToReuqestTab();
        HashMap<String, String> record = myData.get_record(ID).get_map();
        
        apiExplorerPage.sendMatchRequest(record);
        assertResult(ID);
    }
    
    @DataProvider(name = "dataProviderForMatch")
    protected Iterator<Object[]> testProviderForMatch(ITestContext context, Method method) {
        if (method.getName().equals("generatePeopleMatchRequest")) {
            return readSpecifiedRecord("/people/match");
        }

        if (method.getName().equals("generatePlaceMatchRequest")) {
            return readSpecifiedRecord("/places/match");
        }
        
        if (method.getName().equals("generateHouseholdsMatchRequest")) {
            return readSpecifiedRecord("/households/match");
        }
        
        if (method.getName().equals("generateEntitiesMatchRequest")) {
            return readSpecifiedRecord("/entities/match");
        }

        Assert.fail("Unknown method to provide data");
        return null;
    }

    private void dataInitialization() {
        try {
            FileInputStream myInputFile = new FileInputStream(new File("src/test/resources/http_request_input.csv"));
            myData = new DataHandler(myInputFile, ",", true, true, 0);
            myInput = myData.get_map();
            
            FileInputStream myBaselineFile = new FileInputStream(new File("src/test/resources/http_request_baseline.csv"));
            myBaselineData = new DataHandler(myBaselineFile, ",", true, true, 0);
            
          } catch(Exception e) {
            Assert.fail("Problem fetching data from input file:"+e.getMessage());
          }
    }
    
    private Iterator<Object[]> readSpecifiedRecord(String matchName) {
        List<Object[]> test_IDs = new ArrayList<Object[]>();

        for (Map.Entry<String, RecordHandler> entry : myInput.entrySet()) {
            String singular_test_ID = entry.getKey();
            String singular_test_case = entry.getValue().get("TestCase");
            String singular_call_suff = entry.getValue().get("call_suff");

            if (!singular_test_ID.equals("") && !singular_test_case.equals("")
                    && singular_call_suff.equals(matchName)) {
                test_IDs.add(new Object[] {singular_test_ID, singular_test_case});
            }
        }

        return test_IDs.iterator();
    }
    
    private void assertResult(String ID) {
        assertEquals(apiExplorerPage.getTextOfResponseBody(), myBaselineData.get_record(ID).get("response body"));
        assertEquals(apiExplorerPage.getTextOfResponseCode(), myBaselineData.get_record(ID).get("response code"));
        assertEquals(apiExplorerPage.getTextOfResponseHeaders(), myBaselineData.get_record(ID).get("response headers"));
    }
}
