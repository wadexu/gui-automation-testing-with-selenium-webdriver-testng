/**
 * 
 */
package com.acxiom.dsapi.tests;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.acxiom.dsapi.pages.home.HomePage;
import com.acxiom.qa.core.testng.TakeScreenshotOnFailure;
import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

/**
 * @Description: TestHomePage
 * @author wadexu
 *
 * @updateUser
 * @updateDate
 */
public class TestHomePage extends LoginableTestBase {
    private HomePage homePage;

    @BeforeClass(dependsOnMethods = {"beforeClass"})
    public void initTestData() {
      doLogin();
    }

    @BeforeMethod
    public void beforeTest() {
      // init page
      webDriver.get(baseUrl);
      homePage = new HomePage(webDriver);
      assertTrue(homePage.getWelcomeLabel().getText().trim().startsWith("WELCOME, "));
    }
    
    @Test
    @TakeScreenshotOnFailure
    public void testSendYourFirstRequestLink() {
      assertTrue(homePage.getSendYourFirstRequestLink().isDisplayed());
      assertEquals(homePage.getSendYourFirstRequestLink().getText(), "Send Your First Request");
      homePage.getSendYourFirstRequestLink().click();
      assertEquals(webDriver.getCurrentUrl(), baseUrl + "/code/explorer");
    }
    
    @Test
    @TakeScreenshotOnFailure
    public void testDashboardLink() {
      assertTrue(homePage.getDashboardLink().isDisplayed());
      assertEquals(homePage.getDashboardLink().getText(), "Dashboard");
      homePage.getDashboardLink().click();
      waitUntilInvisible(homePage.getLearnPicLocator());
      WaitUntilTitleContains("Acxiom Dashboard");
      assertEquals(webDriver.getCurrentUrl(), baseUrl + "/dashboard");
    }

    @Test
    @TakeScreenshotOnFailure
    public void testNavigateToApplicationPage() {
      actions.moveToElement(homePage.getCodeMainLink());
      waitUntil(1);
      actions.moveToElement(homePage.getGettingStartedSubMenu());
      waitUntil(1);
      actions.moveToElement(homePage.getAddApplicationSubMenu()).click().perform();
      
      assertEquals(webDriver.getCurrentUrl(), baseUrl + "/dashboard/apps/view#/applications");
      
      WebElement button = webDriver.findElement(By.xpath(""));
  
      actions.contextClick(button).perform();
      
    }
    
    @Test
    @TakeScreenshotOnFailure
    public void testNavigateToApplicationPage2() {
      clickWebElement(homePage.getAddApplicationSubMenu()); //another way to handle hide element, execute Javascript directly
      assertEquals(webDriver.getCurrentUrl(), baseUrl + "/dashboard/apps/view#/applications");
    }
}
