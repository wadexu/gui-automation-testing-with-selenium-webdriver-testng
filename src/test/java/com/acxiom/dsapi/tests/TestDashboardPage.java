package com.acxiom.dsapi.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.acxiom.dsapi.pages.dialog.DashboardLayoutDialog;
import com.acxiom.dsapi.pages.dialog.DashboardWidgetDirDialog;
import com.acxiom.dsapi.pages.portal.DashboardPage;
import com.acxiom.dsapi.utils.TableUtils;
import com.acxiom.qa.core.testng.CustomTestListener;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;
@Listeners({CustomTestListener.class})
/**
 * @Description: Test Dashboard Page
 * @author wadexu
 * 
 * @updateUser
 * @updateDate
 */
public class TestDashboardPage extends LoginableTestBase {
    private DashboardPage dashboardPage;
    private DashboardLayoutDialog layoutDialog;
    private DashboardWidgetDirDialog widgetDialog;

    @BeforeClass(dependsOnMethods = {"beforeClass"})
    public void init() {
        doLogin();
        // init page
        webDriver.get(baseUrl + "/dashboard");
        WaitUntilTitleContains("Acxiom Dashboard");
        dashboardPage = new DashboardPage(webDriver);
        widgetDialog = new DashboardWidgetDirDialog(getWebDriver());
        waitUntil(2);

        // init data
        deleteAllWidgets();
        setLayoutToDefault();

    }

    @Test
    public void addMeteringWidget() {
        dashboardPage.clickAddWidgetButton();
        widgetDialog.clickAddMeteringWidgetButton();
        waitUntil(1);
        assertEquals(dashboardPage.getWidgetTitle("Metering"), "Metering");
        List<String> result = TableUtils.getStringValueByColumnPosition(dashboardPage.getFirstMeteringWidgetTableData(), 1);
        waitUntil(1);
        assertEquals(result.get(0), "No data available in table");
        waitUntil(1);
    }

    @Test
    public void addTodayUsageWidget() {
        dashboardPage.clickAddWidgetButton();
        widgetDialog.clickAddUsageWidgetButton();
        waitUntil(1);
        assertEquals(dashboardPage.getWidgetTitle("TodayUsage"), "Today's Usage (Tenant Level)");
        waitUntil(1);
    }
    
    @Test
    public void addAPIStatusWidget() {
        dashboardPage.clickAddWidgetButton();
        widgetDialog.clickAddAPIStatusWidgetButton();
        waitUntil(1);
        assertEquals(dashboardPage.getWidgetTitle("APIStatus"), "API Status");
        waitUntil(1);
    }
    
    @Test
    public void addResponseTimeWidget() {
        dashboardPage.clickAddWidgetButton();
        widgetDialog.clickAddResponseTimeWidgetButton();
        assertEquals(widgetDialog.getWidgetInfoMsg(), "This widget will be supported later.");
        widgetDialog.closeWidgetInfoAlert();
        widgetDialog.clickCloseButton();
        waitUntil(1);

    }
    
    @Test (dependsOnMethods = {"addAPIStatusWidget"}, description = "API Status Widget cannot be added twice")
    public void addAPIStatusWidgetTwice() {
        dashboardPage.clickAddWidgetButton();
        widgetDialog.clickAddAPIStatusWidgetButton();
        assertEquals(widgetDialog.getWidgetErrorMsg(), "This widget is already existed.");
        widgetDialog.closeWidgetErrorAlert();
        widgetDialog.clickCloseButton();
        waitUntil(1);

    }

    // @Test
    // public void setLayoutTo_AAA() {
    // dashboardPage.getLayoutSetButton().click();
    // layoutDialog = new DashboardLayoutDialog(getWebDriver());
    // layoutDialog.getLayout_aaa().click();
    // }

//    @Test
//    public void testDeleteAllWidgets() {
//        deleteAllWidgets();
//    }


    private void setLayoutToDefault() {
        dashboardPage.getLayoutSetButton().click();
        layoutDialog = new DashboardLayoutDialog(getWebDriver());
        layoutDialog.getLayout_a().click();
    }

    private void deleteAllWidgets() {
        try {
            dashboardPage.deleteAllWidgets();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
